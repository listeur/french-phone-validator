<?php

namespace Listeur\FrenchPhoneValidator\Service;

use Symfony\Component\Validator\Exception\UnexpectedTypeException;

class FrenchPhoneService
{
    const COUNTRY_CODE = 33;

    /**
     * @param $value
     * @return string
     */
    public static function cleanPhoneNumber($value)
    {
        if (is_string($value) || is_numeric($value)) {
            return preg_replace('/[^+\d]/i', '', $value);
        } else {
            throw new UnexpectedTypeException($value, 'string|integer');
        }
    }

    /**
     * @param $value
     * @return bool
     */
    public static function isValidFrenchPhone($value)
    {
        if (is_string($value) || is_numeric($value)) {
            return (bool)preg_match('/^(?:\+?\d\d)?0?[1-9](?:\d\d){4}$/i', $value);
        } else {
            throw new UnexpectedTypeException($value, 'string|integer');
        }
    }

    /**
     * @param $value
     * @return string
     */
    public static function format($value)
    {
        $pattern = '/^(?:\+?(\d\d))?0?([1-9])(\d\d)(\d\d)(\d\d)(\d\d)$/i';
        if (self::isValidFrenchPhone($value)) {
            if (preg_match($pattern, $value, $matches)) {
                if ($matches[1] !== '') {
                    return preg_replace($pattern, '+$1 $2 $3 $4 $5 $6', $value);
                } else {
                    return preg_replace($pattern, '+' . self::COUNTRY_CODE . ' $2 $3 $4 $5 $6', $value);
                }
            } else {
                return $value;
            }
        }
    }
}
