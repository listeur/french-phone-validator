<?php

namespace Listeur\FrenchPhoneValidator\Constraint;

use Symfony\Component\Validator\Constraint;

class FrenchPhone extends Constraint
{
    public $message = 'Not a valid phone number';
}
