<?php

namespace Listeur\FrenchPhoneValidator\Constraint;

use Listeur\FrenchPhoneValidator\Service\FrenchPhoneService;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;
use Symfony\Component\Validator\Exception\UnexpectedTypeException;

class FrenchPhoneValidator extends ConstraintValidator
{

    /**
     * Checks if the passed value is valid.
     *
     * @param mixed $value The value that should be validated
     * @param Constraint $constraint The constraint for the validation
     */
    public function validate($value, Constraint $constraint)
    {
        if (!$constraint instanceof FrenchPhone) {
            throw new UnexpectedTypeException($constraint, FrenchPhone::class);
        }

        if (null === $value || '' === $value) {
            return;
        }

        $value = FrenchPhoneService::cleanPhoneNumber($value);

        if (!FrenchPhoneService::isValidFrenchPhone($value)) {
            $this->context->buildViolation($constraint->message)
                ->addViolation();
        }
    }
}